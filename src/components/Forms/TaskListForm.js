import React, { useState } from "react";
import "../../css/Task.css";
import { useForm } from "react-hook-form";
import apiServer from "../../config/apiServer";
import { useParams } from "react-router-dom";

const TaskListForm = ({ setTasklists, showSideTasklistForm }) => {
  const { register, handleSubmit, errors } = useForm();
  const [tasklistName, setTasklistName] = useState();
  const { projectId } = useParams();
  const handleNameChange = (e) => {
    setTasklistName(e.target.value);
  };

  const onSubmit = async ({ name }) => {
    const userId = localStorage.getItem("userId");
    await apiServer.post(`/project/${projectId}/tasklist`, { name, userId });

    const res = await apiServer.get(`/project/${projectId}/tasklists`);
    setTasklists(res.data);
    showSideTasklistForm();
  };

  const handleUserKeyPress = (e) => {
    if (e.key === "Enter" && !e.shiftKey) {
      handleSubmit(onSubmit)();
    }
  };

  return (
    <form
      className="form-container"
      style={{}}
      onSubmit={handleSubmit(onSubmit)}
    >
      <div className="form-top-container">
        <div className="form-section">
          <div className="label-container">
            <label className="form-label">Nome da Coluna</label>
          </div>
          <div className="input-container">
            <input
              name="name"
              type="text"
              placeholder={"Nome da Coluna"}
              className="form-input"
              ref={register({ required: true })}
              onChange={handleNameChange}
              onKeyPress={handleUserKeyPress}
            ></input>
            {errors.name?.type === "required" && (
              <p className="error-message">Please enter a column name</p>
            )}
          </div>
        </div>
      </div>

      <div className="form-button-container">
        <button className="cancel-button" onClick={showSideTasklistForm}>
          Cancelar
        </button>
        <button
          className={
            tasklistName ? "submit-button enabled" : "submit-button disabled"
          }
          disabled={tasklistName ? false : true}
          type="submit"
        >
          Criar Coluna
        </button>
      </div>
    </form>
  );
};

export default TaskListForm;
