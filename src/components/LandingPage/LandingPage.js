/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import "../../css/style.css";
import Hero from "./Hero";
import Services from "./Services";
import Header from "../Header/Header";
import "../../css/LandingPage.css";
import Footer3 from "./Footer";

const LandingPage = () => {
  return (
    <>
      <Header />
      <Hero />
      <Services />
      <Footer3 />
    </>
  );
};

export default LandingPage;
