import React from "react";
import picture from "../../assets/Product-screenshot.png";
import "../../css/LandingPage.css";

export default function Hero() {
  return (
    <section
      className="hero__area hero__height-3 hero__bg p-relative d-flex align-items-center"
      style={{ backgroundImage: "url(/img/home-3/2.png)" }}
    >
      <div className="hero__shape-3">
        <img
          className="hero-3-circle-2"
          src="/img/home-3/hero-circle.png"
          alt=""
        />
        <img className="hero-3-dot" src="/img/home-3/hero-dot.png" alt="" />
        <img className="hero-3-dot-3" src="/img/home-3/hero-dot-3.png" alt="" />
        <img className="hero-3-dot-4" src="/img/home-3/hero-dot-4.png" alt="" />
        <img
          className="hero-3-triangle"
          src="/img/home-3/hero-triangle.png"
          alt=""
        />
      </div>
      <div className="container">
        <div className="row align-items-center">
          <div className="col-xxl-5 col-xl-5 col-lg-6 col-md-6">
            <div className="hero__thumb-3 ">
              <img
                className="hero-phone wow fadeInLeft"
                data-wow-delay=".3s"
                src={picture}
                alt=""
                width="90%"
              />
              <img
                className="hero-3-dot-2"
                src="/img/home-3/hero-dot-2.png"
                alt=""
              />
            </div>
          </div>
          <div className="col-xxl-7  col-lg-6 col-md-6">
            <div className="hero__content-3">
              <h3 className="hero__title-3 wow fadeInUp" data-wow-delay=".3s">
                A maneira mais fácil de gerenciar equipes, projetos e tarefas
              </h3>
              {/* <p className="wow fadeInUp" data-wow-delay=".5s">
                Por que usar o Organizanto? Organizanto te dá tudo que você
                precisa para ficar em sincronia, cumpra os prazos e alcance seus
                objetivos
              </p> */}
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
